# Base image with bare minimum of OS dependencies
# and with NodeJS and npm included
FROM cypress/browsers:node14.16.0-chrome89-ff86

# Setting the working directory to have it contained
WORKDIR /app
# Copy all needed configs and files
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
COPY cypress.json ./cypress.json
COPY reporter-config.json ./reporter-config.json
# Copy the "engine" code
COPY cypress/support ./cypress/support
COPY cypress/plugins ./cypress/plugins
COPY cypress/integration ./cypress/integration

# avoid many lines of progress bars during install
# https://github.com/cypress-io/cypress/issues/1243
ENV CI=1

# Installing dependencies and packages
RUN npm install

# Verify that Cypress has been installed correctly.
# running this command separately from "cypress run" will also cache its result
# to avoid verifying again when running the tests
RUN ./node_modules/.bin/cypress verify
