import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ShowroomPage extends Page {
    /**
     * define selectors using getter methods
     */
    get nonEmptyResult () { return cy.get('[data-e2e-id="Car Offerings Grid Filtered"]'); }

    /**
     * overwrite specifc options to adapt it to page object
     */
    open () {
        return super.open('business/showroom/');
    }

    verifySearchResult() {
        return this.searchResult();
    }

    /**
     * Custom method to verify users' destination
     * @param {*} url 
     */
    verifyDestination(url) {
        return cy.url().should('include', url);
    }

    /**
     * Custom method to verify page's title
     * @param {*} name 
     */
    verifyPageTitle(name) {
        return cy.title().should('include', name);
    }

    /**
     * Method to verify query parameter of the resulting URL
     * after a filter has been selected
     * @param {*} parameter 
     */
    verifyQueryWith(parameter) {
        return cy.url().should('include', parameter);
    }

    /**
     * Method to select a quick filter by selector
     * @param {*} type of the quick filter
     */
    selectQuickFilterOf(type) {
        return cy.get('a[data-key=\'' + type + '\']').click();
    }

    searchResult() {
        this.nonEmptyResult
            .should('be.visible')
            .and('have.attr', 'data-tag-id');
    }
}

export default new ShowroomPage();