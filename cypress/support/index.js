// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';

// Alternatively you can use CommonJS syntax:
// require('./commands')

/**
 * This is to remove the cookie wall on the showroom page
 */
Cypress.on('window:before:load', window => {
    window.document.cookie = 'OptanonAlertBoxClosed=2021-04-10T22:41:38.172Z';
});
