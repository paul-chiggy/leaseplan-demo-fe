/** 
 * Steps definistions for the filtering functionality on the showroom page
 */

import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

import ShowroomPage from '../../../pageobjects/showroom.page.js';

const url = 'https://www.leaseplan.com/en-be/business/showroom/';

/**
 * Regualar beforeAll hook that runs before all scenarios.
 * This saves time and performance.
 */
before(() => {
    ShowroomPage.open();
});

/** 
 * Given steps - this is an already pre-set condition
 */
Given('User opened the showroom page', () => {
    ShowroomPage.verifyDestination(url);
});

/**
 * When steps - action items with users' actions
 */
When('User selects quick filter {string}', (type) => {
    ShowroomPage.selectQuickFilterOf(type);
});

/** 
 * Then steps - rexpected behaviour of the application/functionality.
 */
Then('User should see {string} in the title', (name) => {
    ShowroomPage.verifyPageTitle(name);
});

Then('Showroom page URL should contain {string} parameter', (parameter) => {
    ShowroomPage.verifyQueryWith(parameter);
});

Then('User should see non-empty search results', () => {
    ShowroomPage.verifySearchResult();
});