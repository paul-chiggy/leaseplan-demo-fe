Feature: Filtering products on the showroom page
  I want to be able to filter out products on the page

  Background:
    Given User opened the showroom page
  
  Scenario Outline: Filtering products by quick filters
    When User selects quick filter "<type>"
    Then User should see "<title_name>" in the title
    And Showroom page URL should contain "<query>" parameter
    And User should see non-empty search results

  Examples:
    | type      | title_name          | query                    |
    | Electric  | Business Lease cars | ?fuelTypes=electric      |
    | SUV       | Business Lease cars | ?bodyTypes=suv           |
    | Automatic | Business Lease cars | ?transmissions=automatic |
    | Hybrid    | Business Lease cars | ?fuelTypes=hybrid        |
    | Petrol    | Business Lease cars | ?fuelTypes=petrol        |

